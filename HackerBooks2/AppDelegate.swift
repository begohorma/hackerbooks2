//
//  AppDelegate.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 26/2/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var context :NSManagedObjectContext? //Para poder trabajar con CoreData

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //Create Core Data instance and get context
        let container = persistentContainer(dbName:"HackerBooks2"){ (error:NSError) in
            
            fatalError("Unresolved error \(error), \(error.userInfo)")
        }
        
        self.context = container.viewContext
        
        
        // Create the model

            guard let context = self.context else{
                fatalError("No context available")
            }
            
            let lib:Library = Library(context: context)
            lib.createModel()
        
        
        injectContextToFirstViewController()
        
        return true
    }

    func injectContextToFirstViewController() {
        if let navController = window?.rootViewController as? UINavigationController,
            let initialViewController = navController.topViewController as? LibraryViewController
        {
            initialViewController.context = self.context
            
        }
    }
    
    
    

    func applicationDidEnterBackground(_ application: UIApplication) {
        guard let context = self.context else{
           fatalError("No context available")
        }
        saveContext(context:context)
    }

    
    func applicationWillTerminate(_ application: UIApplication) {
        guard let context = self.context else{
            fatalError("No context available")
        }
        saveContext(context: context)
    }

  

    

}

