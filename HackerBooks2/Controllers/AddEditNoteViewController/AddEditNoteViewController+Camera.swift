//
//  AddEditNoteViewController+Camera.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 14/5/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation
import UIKit

extension AddEditNoteViewController: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func takeAnnotationPhoto(){
        
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            takeNewPhoto()
        }else {
            choosePhotoFromLibrary()
            }
    }
        
    
func takeNewPhoto(){
    let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    
    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    alertController.addAction(cancelAction)
    
    let takePhotoAction = UIAlertAction(title: "Take photo", style: .default) { _ in
        self.takePhotoWithCamera()
    }
    alertController.addAction(takePhotoAction)
    
    let chooseFromLibraryAction = UIAlertAction(title: "Choose from library", style: .default) { _ in
        self.choosePhotoFromLibrary()
    }
    alertController.addAction(chooseFromLibraryAction)
    present(alertController, animated: true, completion: nil)
}

    func takePhotoWithCamera(){
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
func choosePhotoFromLibrary(){
    let imagePicker = UIImagePickerController()
    imagePicker.sourceType = .photoLibrary
    imagePicker.delegate = self
    imagePicker.allowsEditing = true
    present(imagePicker, animated: true, completion: nil)
}
  
    // MARK: -  Delegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let imageTaken = info[UIImagePickerControllerEditedImage] as? UIImage
        self.notePhoto.image = imageTaken
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
