//
//  AddEditNoteViewController+UITextView.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 13/5/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation
import UIKit

extension AddEditNoteViewController: UITextViewDelegate{
   
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n"){
            noteText.resignFirstResponder()
            return false
        }
        return true
    }
    
}
