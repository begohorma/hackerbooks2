//
//  AddEditNoteViewController.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 11/5/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import UIKit
import MapKit

class AddEditNoteViewController: UIViewController {

    
    var book: Book!
    var annotation : Annotation?
    var locationManager: CLLocationManager?
    let geocoder = CLGeocoder()
    
    
    @IBOutlet weak var notePhoto: UIImageView!
    
    @IBOutlet weak var noteText: UITextView!
    
    @IBOutlet weak var mapView: MKMapView!
    
    
    @IBOutlet weak var addressText: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager = CLLocationManager()
        setLocationManager()
        
        noteText.delegate = self
        
        title=book.title
        
        if self.annotation != nil{
            noteText.text = annotation?.text
            addressText.text = annotation?.location?.address
            if  let annotationPhoto = annotation?.photo?.imageData{
                notePhoto.image = UIImage(data: annotationPhoto as Data)
            }
        }else{
            addressText.isHidden = true
        }
       
        
    }


    @IBAction func saveNote(_ sender: Any) {
        guard let context = book.managedObjectContext else {return}
        if self.annotation == nil{
            self.annotation = Annotation(context: context)
            annotation?.creationDate = NSDate()
            if notePhoto.image != nil{
                let annotationPhoto = AnnotationPhoto(context: context)
               annotationPhoto.imageData = UIImagePNGRepresentation(notePhoto.image!)! as NSData
                annotation?.photo = annotationPhoto
            }
            if mapView.userLocation.coordinate.latitude != 0.0{
                let annotationLocation = Location(context: context)
                annotationLocation.longitude = Double(mapView.userLocation.coordinate.longitude)
                annotationLocation.latitude = Double(mapView.userLocation.coordinate.latitude)
                //TODO -  obtener la dirección antes de guardar y ponerla en el campo de texto y al savar coger ese valor
                geocoder.reverseGeocodeLocation(mapView.userLocation.location!, completionHandler: { (placemarks, error) in
                    if error == nil{
                       var address = "unknown address"
                        if let placemark = placemarks?.last{
                            address = self.stringFromPlacemark(placemark:placemark)
                            annotationLocation.address = address
                        }
                        self.addressText.text = address
                        
                        self.annotation?.text = self.noteText.text
                        self.annotation?.modificationDate = NSDate()
                        
                        
                        //asociar lo nota con su book
                        self.annotation?.book = self.book
                        
                        //salvar el contexto
                        saveContext(context: context)
                        
                        //desapilar el vc para volver al anterior
                        let _ = self.navigationController?.popViewController(animated: true)

                    }
                    else{
                        print("Error obteniendo addres \(error.debugDescription)")
                    }
                })
                
                self.annotation?.location = annotationLocation
            }
        }
        else{
                // TODO- Refactorizar esta parte se repite
                if notePhoto.image != nil{
                    let annotationPhoto = AnnotationPhoto(context: context)
                    annotationPhoto.imageData = UIImagePNGRepresentation(notePhoto.image!)! as NSData
                    annotation?.photo = annotationPhoto
                }
                
                        annotation?.text = noteText.text
                        annotation?.modificationDate = NSDate()
                
                
                        //asociar lo nota con su book
                        annotation?.book = book
                
                        //salvar el contexto
                        saveContext(context: context)
                
                        //desapilar el vc para volver al anterior
                        let _ = self.navigationController?.popViewController(animated: true)
            }
        
        

    }

    
 func stringFromPlacemark(placemark: CLPlacemark) -> String{
        var add = ""
        //calle
        if let s = placemark.thoroughfare{
            add += s + ", "
        }
        //numero
        if let s = placemark.subThoroughfare{
            add += s
        }
        //cod Postal
        if let s = placemark.postalCode{
            add += " " + s
        }
        
        //localidad
        if let s = placemark.locality{
            add += " " + s
        }
        //provincia - administrative area
        if let s = placemark.administrativeArea{
            add += " " + s
         }
        //pais
        if let s = placemark.country{
            add += " " + s
        }
    return add
    }
    

    @IBAction func takePhoto(_ sender: Any) {
        self.takeAnnotationPhoto()
    }

    @IBAction func deleteNote(_ sender: Any) {
        guard let context = book.managedObjectContext else {return}
        
                if self.annotation != nil{
                    context.delete(annotation!)
                    saveContext(context: context)
                    
                    //desapilar el vc para volver al anterior
                    let _ = self.navigationController?.popViewController(animated: true)                }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    

    

}
