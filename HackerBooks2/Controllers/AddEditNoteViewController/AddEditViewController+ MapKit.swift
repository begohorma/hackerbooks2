//
//  AddEditViewController+ MapKit.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 13/5/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation
import  MapKit

extension AddEditNoteViewController: CLLocationManagerDelegate{
    
    func setLocationManager(){
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.startUpdatingLocation()
        centerMapView()
        
    }
    func centerMapView(){
        //localización por defecto: Madrid
        let defaulLocation = CLLocation(latitude: 40.416775, longitude: -3.703790)
        let region : MKCoordinateRegion
        

        if annotation?.location?.latitude != nil{
            let annotationLocation = CLLocation(latitude: (annotation?.location?.latitude)!, longitude: (annotation?.location?.longitude)!)
            region = MKCoordinateRegionMakeWithDistance(annotationLocation.coordinate, 1000, 1000)
            mapView.setRegion(region, animated: true)
            let pin = MapPin(coordinate: annotationLocation.coordinate, title: "", subtitle: "")
            mapView.addAnnotation(pin)
        }
        else{
            
            if mapView.userLocation.coordinate.latitude == 0.0 {
                region = MKCoordinateRegionMakeWithDistance(defaulLocation.coordinate, 1000, 1000)
            }
            else{
                region = MKCoordinateRegionMakeWithDistance(mapView.userLocation.coordinate, 1000, 1000)
            }
            
        }
        
        mapView.setRegion(region, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        print(locations.first.debugDescription)
        if self.annotation == nil{
            //si se está creando la nota se actualiza el mapa con la posición actual
            guard let newlocation = locations.last else {return}
            mapView.setRegion(MKCoordinateRegionMakeWithDistance(newlocation.coordinate, 1000, 1000), animated: true)
        }
        
        

        
        
        
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error en Core Location")
    }
}
