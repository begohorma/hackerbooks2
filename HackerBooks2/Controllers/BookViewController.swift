//
//  BookViewController.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 9/5/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import UIKit

class BookViewController: UIViewController {

    var book: Book!
    
    
    @IBOutlet weak var favoriteImage: UIBarButtonItem!
    @IBOutlet weak var coverView: UIImageView!
    
    @IBAction func readBook(_ sender: Any) {
        
    }
    
    @IBAction func switchFavorite(_ sender: Any) {
        //actualizar etiquetas del libro
        book.switchFavorite()
        
        //sincronizar la imagen
        syncFavoriteImage()
        
        //notificar cambio ????

        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        syncWithModel()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func syncWithModel(){
        //asignar la cover
        coverView.image = UIImage(data:(book.photo?.imageData)! as Data)
        //icono favoritos
        syncFavoriteImage()
    }
    
    func syncFavoriteImage(){
        if book.isFavorite(){
            favoriteImage.title = "★"
        }else{
            favoriteImage.title = "☆"
        }
        
        
        
    }
    //prepare for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier{
            if identifier == "ShowPDFSegue"{
                let vc = segue.destination as! PdfViewController
                vc.book = self.book
                
            }
        }
    }

   
}

