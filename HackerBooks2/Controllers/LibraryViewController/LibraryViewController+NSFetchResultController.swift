//
//  LibraryViewController+NSFetchResultController.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 4/3/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation
import CoreData

extension LibraryViewController :NSFetchedResultsControllerDelegate{
    
    var fetchedResultsController: NSFetchedResultsController<BookTag> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
        // DUDA: No funciona bien la ordenación de secciones por ProxyTag ?????
        _fetchedResultsController = NSFetchedResultsController(fetchRequest: BookTag.fetchRequestOrderedByProxytagAndTitle(), managedObjectContext: self.context!, sectionNameKeyPath:"tag.name" , cacheName: nil)//poner nombre de cache distintos
        
       _fetchedResultsController?.delegate = self
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _fetchedResultsController!
    }
    
}
