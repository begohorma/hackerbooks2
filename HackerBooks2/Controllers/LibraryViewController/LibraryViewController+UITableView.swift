//
//  LibraryViewController+UITableView.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 3/3/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation
import UIKit

extension LibraryViewController: UITableViewDelegate,UITableViewDataSource{
    
  // MARK: -  Datasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{

        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects

    }
     
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.name

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HackerBook2BookCell", for: indexPath) as! HackerBooks2BookCell
        let bookTag = self.fetchedResultsController.object(at: indexPath)

        cell.book = bookTag.book!
        return cell
    }
    
}
