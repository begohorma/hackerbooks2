//
//  ViewController.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 26/2/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import UIKit
import CoreData

class LibraryViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    var context: NSManagedObjectContext?
    var _fetchedResultsController: NSFetchedResultsController<BookTag>? = nil

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    //prepare for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier{
            if identifier == "BookDetailSegue"{
                let vc = segue.destination as! BookViewController
                let indexPath = tableView.indexPathForSelectedRow
                let bookTag = fetchedResultsController.object(at:indexPath!)
                vc.title = bookTag.book?.title
                vc.book = bookTag.book
                
                
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
        

    
}



