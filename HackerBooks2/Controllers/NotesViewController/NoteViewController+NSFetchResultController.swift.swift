//
//  NoteViewController+NSFetchResultController.swift.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 11/5/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation
import CoreData

extension NotesViewController: NSFetchedResultsControllerDelegate{
    var fetchedResultsController: NSFetchedResultsController<Annotation> {
        if _fetchedResultsController != nil {
            return _fetchedResultsController!
        }
        
    
        _fetchedResultsController = NSFetchedResultsController(fetchRequest: Annotation.annotationsFrom(book: self.book), managedObjectContext: self.context!, sectionNameKeyPath: nil, cacheName: self.book.title)//poner nombre de cache distinto para cada libro
        _fetchedResultsController?.delegate = self
        
        do {
            try _fetchedResultsController!.performFetch()
        } catch {
            let nserror = error as NSError
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
        
        return _fetchedResultsController!
    }
    
    // MARK: - NSFetchedResultController delegate
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
        
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        switch type {
        case .insert:
            self.collectionView.insertSections(IndexSet(integer: sectionIndex))
        case .delete:
            self.collectionView.deleteSections(IndexSet(integer: sectionIndex))
        default:
            return
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            collectionView.insertItems(at: [newIndexPath!])
            //actulizar map
            self.addAnnotationPinsToMap()
        case .delete:
            collectionView.deleteItems(at: [indexPath!])
           self.addAnnotationPinsToMap()
        case .update:
            collectionView.reloadItems(at: [indexPath!])
            self.addAnnotationPinsToMap()
        default:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        
    }

}
