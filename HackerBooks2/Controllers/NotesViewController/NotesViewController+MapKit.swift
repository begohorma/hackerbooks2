//
//  NotesViewController+MapKit.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 13/5/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation
import  MapKit

extension NotesViewController: CLLocationManagerDelegate{
    
    func setLocationManager(){
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.startUpdatingLocation()
        if (self.fetchedResultsController.fetchedObjects?.count)! > 0 {
           addAnnotationPinsToMap()
        }else{
          centerMapView()
        }

        
        
            }
    func centerMapView(){
        //localización por defecto: Madrid
        let defaulLocation = CLLocation(latitude: 40.416775, longitude: -3.703790)
        let region : MKCoordinateRegion
        
        if mapView.userLocation.coordinate.latitude == 0.0 {
            region = MKCoordinateRegionMakeWithDistance(defaulLocation.coordinate, 1000, 1000)
        }
        else{
            region = MKCoordinateRegionMakeWithDistance(mapView.userLocation.coordinate, 1000, 1000)
        }
       
        mapView.setRegion(region, animated: true)
        
    }
    
    func addAnnotationPinsToMap(){
        let annotations = self.fetchedResultsController.fetchedObjects
       
        var pins = [MKAnnotation]()
    
        for ann in annotations! {
            if ann.location != nil {
            let index = ann.text?.index((ann.text?.startIndex)!, offsetBy: 10)
            let pin = MapPin(coordinate: CLLocationCoordinate2D(latitude: (ann.location?.latitude)!,longitude: (ann.location?.longitude)!), title: (ann.text?.substring(to: index!))!, subtitle: ann.formatedDate())
            pins.append(pin)
            }
        }
    mapView.showAnnotations(pins, animated: true)
            

    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        print(locations.first.debugDescription)
        if (self.fetchedResultsController.fetchedObjects?.count)! == 0{
            guard let newlocation = locations.last else {return}
            mapView.setRegion(MKCoordinateRegionMakeWithDistance(newlocation.coordinate, 1000, 1000), animated: true)
        }

    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error en Core Location")
    }
}
