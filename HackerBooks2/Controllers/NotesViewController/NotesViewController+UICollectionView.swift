//
//  NotesViewController+UICollectionView.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 11/5/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation
import UIKit


extension NotesViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.fetchedResultsController.sections?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let sectionInfo = self.fetchedResultsController.sections![section]
        return sectionInfo.numberOfObjects
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //crear las celdas
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HackerBooks2NoteCell", for: indexPath) as! HackerBooks2NoteCell
        
        cell.annotation = self.fetchedResultsController.object(at: indexPath)
        
        return cell
    }
}
