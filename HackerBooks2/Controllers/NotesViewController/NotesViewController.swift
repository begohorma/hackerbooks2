//
//  NotesViewController.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 11/5/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import UIKit
import CoreData
import MapKit

class NotesViewController: UIViewController {

    var book: Book!
    
    var context : NSManagedObjectContext?
    var _fetchedResultsController: NSFetchedResultsController<Annotation>? = nil
    var locationManager: CLLocationManager?
  
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       title=book.title
        self.context = book.managedObjectContext
        
        locationManager = CLLocationManager()
        setLocationManager()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //prepare for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier{
            if identifier == "AddNoteSegue"{
                let vc = segue.destination as! AddEditNoteViewController
                vc.book = self.book
            }
        }
        if let identifier = segue.identifier{
            if identifier == "EditNoteSegue"{
                let vc = segue.destination as! AddEditNoteViewController
                vc.book = self.book
                //identificar nota seleccionada
                let indexPath = collectionView.indexPathsForSelectedItems?.first
                let annotation = fetchedResultsController.object(at: indexPath!)
                vc.annotation = annotation
            }
        }
    }

}

