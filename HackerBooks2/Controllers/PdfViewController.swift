//
//  PdfViewController.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 10/5/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import UIKit

class PdfViewController: UIViewController {

    var book: Book!
    
    @IBOutlet weak var browser: UIWebView!
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    
   
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        browser.delegate = self
        self.title = book.title
        browser.load((book.pdf?.data)! as Data, mimeType: "application/pdf", textEncodingName: "utf8",
                     baseURL: URL(string:(book.pdf?.url)!)!)
        
        //comprobar que el pdf guardado no es el de por defecto
        let defaultPdf = Bundle.main.url(forResource: "emptyPdf", withExtension: "pdf")
        if (book.pdf?.data?.isEqual(to: try! Data(contentsOf: defaultPdf!)))!{
            downloadPdf(ofBook: self.book!) { (pdfData:Data) in
                assert(Thread.current == Thread.main)
                self.book.pdf?.data = pdfData as NSData
               self.hideSpinner()
                self.browser.load((self.book.pdf?.data)! as Data, mimeType: "application/pdf", textEncodingName: "utf8",
                                  baseURL: URL(string:(self.book.pdf?.url)!)!)
            }
        }
        

        
    }

    private func downloadPdf(ofBook book: Book, completion: @escaping (Data) -> Void) {
        showSpinner()
        DispatchQueue.global().async {
            if let url = URL(string: (book.pdf?.url)!) {
                do {
                    
                    let data = try Data(contentsOf: url)
                    
                    DispatchQueue.main.async {
                        completion(data)
                    }
                } catch {
                    print("Error in downloadPdf " + error.localizedDescription)
                }
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //prepare for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier{
            if identifier == "showBookNotesSegue"{
                let vc = segue.destination as! NotesViewController
                vc.book = self.book
                
            }
            
        }
    }
    
}

// MARK: -  UIWebViewDelegate
extension PdfViewController: UIWebViewDelegate{
    func webViewDidStartLoad(_ webView: UIWebView) {
        showSpinner()
            }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
       hideSpinner()
    }
    
    func showSpinner(){
        //mostrar el espiner y activarlo
        spinner.isHidden = false
        spinner.startAnimating()
    }
    
    func hideSpinner(){
        //ocultar el spinner y pararlo
        spinner.isHidden = true
        spinner.stopAnimating()
    }
}
