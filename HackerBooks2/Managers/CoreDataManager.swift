//
//  CoreDataManager.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 26/2/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import CoreData


public func persistentContainer(dbName: String,onError: ((NSError)-> Void)? = nil ) -> NSPersistentContainer {
    let container = NSPersistentContainer(name: dbName)
    
    container.loadPersistentStores(completionHandler: { (storeDescription, error) in
        if let error = error as NSError?,
            let onError = onError {
            onError(error)
        }
    })
    return container
}

public func saveContext(context: NSManagedObjectContext,onError: ((NSError)-> Void)? = nil) {
    if context.hasChanges {
        do {
            try context.save()
        } catch {
            if let onError = onError{
                onError(error as NSError)
            }
        }
    }
}



