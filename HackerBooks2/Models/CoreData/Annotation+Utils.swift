//
//  Annotation+Utils.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 5/3/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation
import CoreData

extension Annotation{
    
    class func annotationsFrom(book:Book) -> NSFetchRequest<Annotation> {
        let fetchRequest: NSFetchRequest<Annotation> = Annotation.fetchRequest()
        fetchRequest.fetchBatchSize = 50
        //ordendo por fecha creacción ascendente
        let sortDescriptor = NSSortDescriptor(key: "creationDate", ascending: true)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        //filtrar
    
        fetchRequest.predicate = NSPredicate(format: "book = %@", book)
        
        return fetchRequest
    }
    
    func formatedDate() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: "es_ES")
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.dateStyle = .short
        return dateFormatter.string(from: self.creationDate! as Date)
    }
    
}
