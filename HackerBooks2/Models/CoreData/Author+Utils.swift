//
//  Author+Utils.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 5/3/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation
import CoreData

extension Author{
    
    

static func exists(fullname: String, context: NSManagedObjectContext)-> Author?{
    do{
        let search = try context.fetch(fetchRequestBy(fullname: fullname))
        
        if search.count > 0{
            return search.first
        }else{
            return nil
        }
        
    }catch{
        return nil
    }
}

    class func fetchRequestBy(fullname: String) -> NSFetchRequest<Author> {
        let fetchRequest: NSFetchRequest<Author> = Author.fetchRequest()
        fetchRequest.fetchBatchSize = 1
        
        fetchRequest.predicate = NSPredicate(format: "fullName == %@", fullname)
        
        return fetchRequest
    }
}
