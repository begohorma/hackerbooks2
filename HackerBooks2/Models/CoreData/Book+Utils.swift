//
//  Book+Utils.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 4/3/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation
import CoreData

extension Book{
    
    
    static func exists(title: String, context: NSManagedObjectContext)-> Book?{
        do{
            let search = try context.fetch(fectchRequestBy(title: title))
            
            if search.count > 0{
                return search.first
            }else{
                return nil
            }
            
        }catch{
            return nil
        }
    }
    
    class func fectchRequestBy(title:String) -> NSFetchRequest<Book>{
        let fetchRequest: NSFetchRequest<Book> = Book.fetchRequest()
        fetchRequest.fetchBatchSize = 1
        fetchRequest.predicate = NSPredicate(format: "title = %@", title)
        return fetchRequest
        
    }
    class func fetchRequestOrderedByTitle() -> NSFetchRequest<Book> {
        let fetchRequest: NSFetchRequest<Book> = Book.fetchRequest()
        fetchRequest.fetchBatchSize = 20
        
        let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        return fetchRequest
    }
    
    func formattedListOfAuthors()-> String{
        //transform Nset into Array and then it into string separated with ,
        var authorsNames: [String] = []
        for auth in self.authors!{
            authorsNames.append((auth as! Author).fullName!)
        }
        
        return authorsNames.sorted().joined(separator: ", ")
    }
    
    func formattedListOfTags()-> String{
        //transform Nset into Array and then it into string separated with ,
        var tagsNames: [String] = []
        for tag in self.booktags!{
            tagsNames.append((tag as! BookTag).name!)
        }
        
        return tagsNames.sorted().joined(separator: ", ")
    }
    
    func isFavorite() -> Bool{
        let bookTag = BookTag.exists(name: "Favorite", title: self.title!, context: self.managedObjectContext!)
        if(bookTag == nil){
            return false
        }else{
            return true
        }

    }

    func favoriteTag() -> BookTag?{
        let bookTag = BookTag.exists(name: "Favorite", title: self.title!, context: self.managedObjectContext!)
        if(bookTag == nil){
            return nil
        }else{
            return bookTag
        }
        
        
    }

    func switchFavorite(){
        if let favTag = favoriteTag(){
             //remove
            self.managedObjectContext?.delete(favTag)
        }
        else{
            //insert
            let favoriteTag = BookTag(context: self.managedObjectContext!)
            favoriteTag.name = "Favorite"
            //create a Tag entity if doesn't already exists one with the same name
            var tag: Tag?
            tag = Tag.exists(name: "Favorite", context: self.managedObjectContext!)
            if(tag == nil){
                tag = Tag(context: self.managedObjectContext!)
                tag?.name = "Favorite"
                tag?.proxyForSorting = "-- FAVORITE" // Al ordenar por este campo este valor debería ser el primero
            }
            //relations
            favoriteTag.book = self
            favoriteTag.tag = tag
            self.addToBooktags(favoriteTag)
            tag?.addToBookTags(favoriteTag)
            
        }
        saveContext(context: self.managedObjectContext!)

    }
    
}
