//
//  BookTag.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 5/3/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation
import CoreData

extension BookTag{

    static func exists(name: String, title:String,context: NSManagedObjectContext)-> BookTag?{
        do{
            let search = try context.fetch(fetchRequestBy(name: name, title: title))
            
            if search.count > 0{
                return search.first
            }else{
                return nil
            }
            
        }catch{
            return nil
        }
    }
    
    
    
    class func fetchRequestBy(name: String, title: String) -> NSFetchRequest<BookTag> {
        let fetchRequest: NSFetchRequest<BookTag> = BookTag.fetchRequest()
        fetchRequest.fetchBatchSize = 1
    
        fetchRequest.predicate = NSPredicate(format: "%K == %@ AND name == %@", "book.title",title,name)
        
        return fetchRequest
    }
    
    
    class func fetchRequestOrderedByProxytagAndTitle() -> NSFetchRequest<BookTag> {
        let fetchRequest: NSFetchRequest<BookTag> = BookTag.fetchRequest()
        fetchRequest.fetchBatchSize = 50
        
        let sortDescriptorTag = NSSortDescriptor(key: "tag.proxyForSorting", ascending: true)
        let sortDescriptorTitle = NSSortDescriptor(key: "book.title", ascending: true)
        
        fetchRequest.sortDescriptors = [sortDescriptorTag,sortDescriptorTitle]
        
        return fetchRequest
    }
}

