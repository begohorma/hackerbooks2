//
//  Pdf+Utils.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 5/3/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation
import CoreData

extension Pdf{

    
    static func exists(url: String, context: NSManagedObjectContext)-> Pdf?{
        do{
            let search = try context.fetch(fectchRequestBy(url: url))
            
            if search.count > 0{
                return search.first
            }else{
                return nil
            }
            
        }catch{
            return nil
        }
    }
    
    class func fectchRequestBy(url:String) -> NSFetchRequest<Pdf>{
        let fetchRequest: NSFetchRequest<Pdf> = Pdf.fetchRequest()
        fetchRequest.fetchBatchSize = 1
        fetchRequest.predicate = NSPredicate(format: "url = %@", url)
        return fetchRequest
        
    }

}
