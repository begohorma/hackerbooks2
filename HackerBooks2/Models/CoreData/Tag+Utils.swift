//
//  Tag+Utils.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 5/3/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation
import CoreData

extension Tag{
    
    static func exists(name: String, context: NSManagedObjectContext)-> Tag?{
        do{
            let search = try context.fetch(fetchRequestBy(name: name))
            
            if search.count > 0{
                return search.first
            }else{
                return nil
            }
            
        }catch{
            return nil
        }
    }
    
    class func fetchRequestBy(name: String) -> NSFetchRequest<Tag> {
        let fetchRequest: NSFetchRequest<Tag> = Tag.fetchRequest()
        fetchRequest.fetchBatchSize = 1
        
        fetchRequest.predicate = NSPredicate(format: "name == %@", name)
        
        return fetchRequest
    }
}
