//
//  Tags.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 27/2/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation

typealias Tags = Set<TagJSON>
typealias TagName = String

struct TagConstants{
    static let favoriteTag = "Favorite"
}

struct TagJSON {
    
    let _name : TagName
    
    init(name: TagName) {
        _name = name.capitalized
    }
    
    static func favoriteTag()-> TagJSON {
        return self.init(name: TagConstants.favoriteTag)
    }
    
    func isFavorite()->Bool{
        return _name == TagConstants.favoriteTag
    }
}

//MARK: - Hashable
// Since tags will go into a MultiDictionary, they must be hashable
extension TagJSON: Hashable{
    public var hashValue: Int {
        return _name.hashValue
    }
}

//MARK: - Equatable
// To be hashable, you must be equatable
// As of swift 3.0, operators can be declared in
// extension
extension TagJSON: Equatable{
    static func ==(lhs: TagJSON, rhs: TagJSON) -> Bool{
        return (lhs._name == rhs._name)
    }
}


//MARK: - Comparable
extension TagJSON: Comparable{
    static func <(lhs: TagJSON, rhs: TagJSON) -> Bool{
        
        if lhs.isFavorite(){
            return true
        }
        else if rhs.isFavorite(){
            return false
        }else{
            return lhs._name < rhs._name
        }
    }
    
}
