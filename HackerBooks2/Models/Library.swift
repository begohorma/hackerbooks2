//
//  Library.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 5/3/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation
import CoreData

class Library{
   

 var context :NSManagedObjectContext? = nil

    init(context : NSManagedObjectContext){
        self.context = context
    }

 func createModel(onError: ((NSError)-> Void)?=nil){
    
    //Get initial data from JSON
    
    // Clean up all local caches
    AsyncData.removeAllLocalFiles()
    
    do {
        guard let url = Bundle.main.url(forResource: "books_readable", withExtension: "json") else{
            fatalError("Unable to read json file!")
        }
        
        let data = try Data(contentsOf: url)
        let jsonDicts = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? JSONArray
        
        let books = try decode(books: jsonDicts)
        
        loadData(books: books)
        
        
    } catch {
            print(error)
        }
    

}

// MARK: -  Json To DB

func loadData(books: [BookJSON]){
    
    //check if books are already loaded - userdefaults
    let userDefaults = UserDefaults.standard
    
    let isPreloaded = userDefaults.bool(forKey: "isPreloaded")
    if !isPreloaded{
        preloadData(books:books)
        userDefaults.set(true,forKey: "isPreloaded")
    }
}


    func preloadData(books: [BookJSON]){
        guard let context = self.context else{
            fatalError("No context available")
        }
        removeData() //TODO
  
        for bookJSON in books{
        
            //create a Book entity if doesn't alredy exists one with the same title
            var book : Book?
            book = Book.exists(title: bookJSON.title,context: context)
            
            if( book == nil){
                book = Book(context: context)
                book?.title = bookJSON.title
            }
            
            // create a Pdf entity if doesn't already exists one with the same url
            var pdf : Pdf?
            let u = bookJSON.pdf.url.absoluteString
            
            pdf = Pdf.exists(url: u, context: context)
            if( pdf == nil){
                pdf = Pdf(context: context)
                pdf?.url = u
                pdf?.data = bookJSON.pdf._data as NSData  //Default pdf data
            }
            //relations
            pdf?.book = book
            
            // create a CoverPhoto entity if doesn't already exists one with the same url
            var coverPhoto : CoverPhoto?
            coverPhoto = CoverPhoto.exists(url: bookJSON.image.url.absoluteString, context: context)
            if(coverPhoto == nil){
                coverPhoto = CoverPhoto(context: context)
                coverPhoto?.url = bookJSON.image.url.absoluteString
                coverPhoto?.imageData = bookJSON.image._data as NSData //default cover image
            }

            //relations
            coverPhoto?.book = book

            
            //authors
            for auth in bookJSON._authors{
                //create a Author entity if doesn't alredy exists one with the same fullname
                var author: Author?
                author = Author.exists(fullname: auth, context: context)
                if(author == nil){
                    author = Author(context: context)
                    author?.fullName = auth
                }
                //relations
                author?.addToBooks(book!)
                book?.authors?.adding(author!)
            }
            
            //tags
            for tg in bookJSON._tags{
                //create a BookTag entity if doesn't alredy exists one with the same name and for the same book title
                var bookTag = BookTag.exists(name: tg._name, title: (book?.title)!, context: context)
                if(bookTag == nil){
                    bookTag = BookTag(context: context)
                    bookTag?.name = tg._name
                }
                
                //create a Tag entity if doesn't already exists one with the same name
                var tag: Tag?
                tag = Tag.exists(name: tg._name, context: context)
                if(tag == nil){
                    tag = Tag(context: context)
                    tag?.name = tg._name
                    tag?.proxyForSorting = tg._name
                }
                
                //relations
                bookTag?.book = book
                bookTag?.tag = tag
                book?.addToBooktags(bookTag!)
                tag?.addToBookTags(bookTag!)
                
            }
        }
        saveContext(context: context) { (error:NSError) in
            
            fatalError("Unresolved error \(error), \(error.userInfo)")    }
        
    }//fin preloadData

func preloadData2(books: [BookJSON]) {
    guard let context = self.context else{
        fatalError("No context available")
    }
        removeData() //TODO
    
    
    for bookJSON in books{
        
        //create Book Entity
        let book = Book(context: context)
        book.title = bookJSON.title
        
        //pdf
        //create PDF Entity
        let pdf = Pdf(context: context)
        pdf.url = bookJSON._pdf.url.absoluteString
        pdf.data = bookJSON._pdf._data as NSData  //Default pdf data
        
        //relations
        pdf.book = book
        
        //coverPhoto
        //create coverPhoto Entity
        let coverPhoto = CoverPhoto(context: context)
        coverPhoto.url = bookJSON._image.url.absoluteString
        coverPhoto.imageData = bookJSON._image._data as NSData //default cover image
        
        
        //authors
        for auth in bookJSON._authors{
            //crate Author entity
            let author = Author(context: context)
            author.fullName = auth
            
            //relations
            author.addToBooks(book)
            book.authors?.adding(author)
        }
        
        //tags
        for tg in bookJSON._tags{
           //create BookTag Entity
            let bookTag = BookTag(context: context)
            bookTag.name = tg._name
            
           //create Tag Entity
            let tag = Tag(context: context)
            tag.name = tg._name
            tag.proxyForSorting = tg._name
            
            //relations
            bookTag.book = book
            bookTag.tag = tag
            book.addToBooktags(bookTag)
            tag.addToBookTags(bookTag)
        }
        
        
    }
    
    saveContext(context: context) { (error:NSError) in
        
        fatalError("Unresolved error \(error), \(error.userInfo)")    }
    
    }
    
func removeData(){
// TODO
//    remove book
//remove authors
//    removetags
//    guard let context = self.context else{
//        return
    }
   
    


}
