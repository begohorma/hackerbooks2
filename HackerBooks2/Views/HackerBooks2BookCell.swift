//
//  HackerBooks2BookCell.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 4/3/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import Foundation
import UIKit


class HackerBooks2BookCell:UITableViewCell{
 
    
   
    @IBOutlet weak var coverView: UIImageView!
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var authorsView: UILabel!
    @IBOutlet weak var tagsView: UILabel!
    
    var _book:Book?=nil
    
    var book:Book{
        get{
           return _book!
        }
        set{
            _book = newValue
            
            syncWithBook()
        
        }
        
    }
    
    private func syncWithBook(){
        
        coverView.image = UIImage(data: (self._book?.photo?.imageData)! as Data)
        titleView.text = self._book?.title
        authorsView.text = self._book?.formattedListOfAuthors()
        tagsView.text = self._book?.formattedListOfTags()
        
        
        //descargar la cover en background y mostrarla con una animación cuando este disponible
        //comprobar que la cover guardada no es la de por defecto
        let defaultCover = Bundle.main.url(forResource: "emptyBookCover", withExtension: "png")
        if (self._book?.photo?.imageData?.isEqual(to: try! Data(contentsOf: defaultCover!)))!{
            downloadCover(ofBook: self._book!) { (cover:UIImage) in
            assert(Thread.current == Thread.main)
            UIView.transition(with: self.coverView,
                              duration: 0.7,
                              options: [.curveEaseOut],
                              animations: { 
                                self.coverView.image = cover
                }, completion: nil)
        
            }
        
        }
        
        
    }
    
    private func downloadCover(ofBook book: Book, completion: @escaping (UIImage) -> Void) {
        
        DispatchQueue.global().async {
            if let url = URL(string: (book.photo?.url)!) {
                do {
                    
                    let data = try Data(contentsOf: url)
                    let image = UIImage(data: data)
                    
                    DispatchQueue.main.async {
                        completion(image!)
                    }
                } catch {
                    print("Error in downloadCover " + error.localizedDescription)
                }
            }
        }
    }

}
