//
//  HackerBooks2NoteCell.swift
//  HackerBooks2
//
//  Created by Begoña Hormaechea on 11/5/17.
//  Copyright © 2017 Begoña Hormaechea. All rights reserved.
//

import UIKit

class HackerBooks2NoteCell: UICollectionViewCell {
   
    @IBOutlet weak var notePhoto: UIImageView!
    
    @IBOutlet weak var noteText: UILabel!
    
    @IBOutlet weak var locationImage: UIImageView!
    
    
    @IBOutlet weak var creationDateLabel: UILabel!
    
    var _annotation:Annotation? = nil
    
    var annotation:Annotation {
        get{
            return _annotation!
        }
        set{
            _annotation = newValue
            
            
            
            syncWithAnnotation()
            
        }
        
    }
    
    
    private func syncWithAnnotation(){
        noteText.text = self.annotation.text
        creationDateLabel.text = self.annotation.formatedDate()

        if let photoData = self.annotation.photo?.imageData{
            notePhoto.image = UIImage(data:(photoData) as Data)
        }else{
            notePhoto.image = #imageLiteral(resourceName: "noImage.png")
        }
        
        if self.annotation.location?.latitude != nil{
            self.locationImage.isHidden = false
        
        }else{
            self.locationImage.isHidden = true
        }

    }
}
